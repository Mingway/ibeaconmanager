//
//  keychainUtils.h
//  zxjyw
//
//  Created by HelyData on 13-9-22.
//  Copyright (c) 2013年 HelyData. All rights reserved.
// 钥匙串存储

#import <Foundation/Foundation.h>

@interface keychainUtils : NSObject
+ (NSMutableDictionary *)getKeychainQuery:(NSString *)service;
+ (void)save:(NSString *)service data:(id)data;
+ (id)load:(NSString *)service;
+ (void)delete:(NSString *)service;
@end
