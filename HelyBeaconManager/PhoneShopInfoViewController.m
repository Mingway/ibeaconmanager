//
//  PhoneShopInfoViewController.m
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneShopInfoViewController.h"
#import "AppDelegate.h"
#import "MBProgressHUD.h"
#import "UIImage+fixOrientation.h"
#import "PhoneUpdateViewController.h"

#define KEY_NAME            @"shopName"
#define KEY_TEL             @"shopTel"
#define KEY_ADDRESS         @"shopAddress"
#define KEY_DESCRIPTION     @"shopDescription"

@interface PhoneShopInfoViewController ()<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    MBProgressHUD *_progressHUD;
    UIImagePickerController *_picker;
    ShopInfo *_privateShopInfo;
    
    BOOL _isUpdateImage;
}

@end

@implementation PhoneShopInfoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (!_picker) {
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.allowsEditing = YES;
    }
    
    [self getShopInfo];
    
    UIBarButtonItem *saveBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleBordered target:self action:@selector(update)];
    self.navigationItem.rightBarButtonItem = saveBarButtonItem;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:KEY_NAME]){
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }else if([keyPath isEqualToString:KEY_TEL]){
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([keyPath isEqualToString:KEY_ADDRESS]){
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([keyPath isEqualToString:KEY_DESCRIPTION]) {
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:3 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)getShopInfo{
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    
    
    [ApplicationDelegate.helyEngine shopInfoForShopId:self.shopInfo.shopId completionHandler:^(BOOL isSuccess, NSDictionary *shopInfoDic) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            [self.shopInfo updateWithDic:shopInfoDic];
            if (!_privateShopInfo) {
                _privateShopInfo = [[ShopInfo alloc]init];
                [_privateShopInfo addObserver:self forKeyPath:KEY_NAME options:NSKeyValueObservingOptionNew context:nil];
                [_privateShopInfo addObserver:self forKeyPath:KEY_TEL options:NSKeyValueObservingOptionNew context:nil];
                [_privateShopInfo addObserver:self forKeyPath:KEY_ADDRESS options:NSKeyValueObservingOptionNew context:nil];
                [_privateShopInfo addObserver:self forKeyPath:KEY_DESCRIPTION options:NSKeyValueObservingOptionNew context:nil];
            }
            [_privateShopInfo updateWithDic:shopInfoDic];
            
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        return 140;
    }else{
        CGFloat height = 0.0f;
        NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
        if (row == 0) {
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [_privateShopInfo.shopName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }else if (row == 1){
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [_privateShopInfo.shopTel boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }else if (row == 2){
            CGSize labelMaxSize = CGSizeMake(260, 1000);
            CGRect labelBounds = [_privateShopInfo.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }else if (row == 3){
            CGSize labelMaxSize = CGSizeMake(260, 1000);
            CGRect labelBounds = [_privateShopInfo.shopDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }
        
        return height;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return 4;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
        
        UIImageView *imageView = (UIImageView*)[cell viewWithTag:1];
        [imageView setImageFromURL:_privateShopInfo.imageUrl placeHolderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        return cell;
    }else{
        if (row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopNameCell" forIndexPath:indexPath];
            
            UILabel *nameLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [_privateShopInfo.shopName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [nameLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            nameLabel.text = _privateShopInfo.shopName;
            
            return cell;
        }else if(row == 1){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopTelCell" forIndexPath:indexPath];
            
            UILabel *telLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [_privateShopInfo.shopTel boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [telLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            telLabel.text = _privateShopInfo.shopTel;
            
            return cell;
        }else if(row == 2){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopAddressCell" forIndexPath:indexPath];
            
            UILabel *addressLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 1000);
            CGRect labelBounds = [_privateShopInfo.shopAddress boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [addressLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            addressLabel.text = _privateShopInfo.shopAddress;
            
            return cell;
        }else{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopDescriptionCell" forIndexPath:indexPath];
            
            UILabel *descriptionLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 1000);
            CGRect labelBounds = [_privateShopInfo.shopDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [descriptionLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            descriptionLabel.text = _privateShopInfo.shopDescription;
            
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    
    if (section == 0) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"选择图片来源" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"拍照",@"相册", nil];
        [actionSheet showInView:self.view];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{
            
        }
            break;
        case 1:{
            //拍照
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
            if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
                sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
            _picker.sourceType = sourceType;
            [self presentViewController:_picker animated:YES completion:nil];
        }
            break;
        case 2:{
            //相册
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            _picker.sourceType = sourceType;
            [self presentViewController:_picker animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark –
#pragma mark Camera View Delegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *newImage = [image fixOrientation];
    
    UIImageView *imageView = (UIImageView*)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:1];
    imageView.image = newImage;
    
    _isUpdateImage = YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UPDATE

- (void)update{
    NSString *base64Str = nil;
    if (_isUpdateImage) {
        UIImageView *imageView = (UIImageView*)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:1];
        NSData *data;
        data = UIImageJPEGRepresentation(imageView.image, 0.5);
        base64Str = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    }
    
    [_progressHUD show:YES];
    
    [ApplicationDelegate.helyEngine updateShopInfoForShopId:_privateShopInfo.shopId shopName:_privateShopInfo.shopName description:_privateShopInfo.shopDescription shopAddress:_privateShopInfo.shopAddress tel:_privateShopInfo.shopTel resData:base64Str completionHandler:^(BOOL isSuccess, NSDictionary *shopInfoDic) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"修改成功" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            
            [self.shopInfo updateWithDic:shopInfoDic];
            _isUpdateImage = NO;
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"shopName"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeShopName];
        [[segue destinationViewController] setShopInfo:_privateShopInfo];
    }else if ([segue.identifier isEqualToString:@"shopTel"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeShopTel];
        [[segue destinationViewController] setShopInfo:_privateShopInfo];
    }else if([segue.identifier isEqualToString:@"shopAddress"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeShopAddress];
        [[segue destinationViewController] setShopInfo:_privateShopInfo];
    }else if([segue.identifier isEqualToString:@"shopDescription"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeShopDescription];
        [[segue destinationViewController] setShopInfo:_privateShopInfo];
    }
}

-(void)dealloc{
    if (_privateShopInfo) {
        [_privateShopInfo removeObserver:self forKeyPath:KEY_NAME context:nil];
        [_privateShopInfo removeObserver:self forKeyPath:KEY_TEL context:nil];
        [_privateShopInfo removeObserver:self forKeyPath:KEY_ADDRESS context:nil];
        [_privateShopInfo removeObserver:self forKeyPath:KEY_DESCRIPTION context:nil];
        _privateShopInfo = nil;
    }
}

@end
