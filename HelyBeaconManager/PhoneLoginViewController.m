//
//  PhoneLoginViewController.m
//  HelyBeaconManager
//
//  Created by developer on 14-4-22.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneLoginViewController.h"
#import "MBProgressHUD.h"
#import "ShopInfo.h"
#import "Utils.h"
#import "PhoneShopListViewController.h"
#import "DataModel.h"
#import "AppDelegate.h"

@import QuartzCore;

static NSString * const KEY_USER = @"com.helydata.app.user";
static NSString * const KEY_PASS = @"com.helydata.app.pass";

@interface PhoneLoginViewController ()<UITextFieldDelegate>{
    MBProgressHUD *_progressHUD;
    BOOL _isTextFieldUp;
}
@property (weak, nonatomic) IBOutlet UITextField *userTextField;
@property (weak, nonatomic) IBOutlet UITextField *passTextField;
- (IBAction)login:(id)sender;

@end

@implementation PhoneLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTapGesture:)];
    [self.view addGestureRecognizer:tap];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardDidShowNotification
                                              object:nil];
    //注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardDidHideNotification
                                              object:nil];
    [super viewWillAppear:YES];
    
    self.userTextField.text = @"";
    self.passTextField.text = @"";
    NSDictionary *storeUserInfo = [Utils read];
    if (storeUserInfo) {
        self.userTextField.text = [storeUserInfo objectForKey:KEY_USER];
        self.passTextField.text = [storeUserInfo objectForKey:KEY_PASS];
        [self login];
    }
    _isTextFieldUp = NO;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.userTextField resignFirstResponder];
    [self.passTextField resignFirstResponder];
}

//监听事件
- (void)handleKeyboardDidShow:(NSNotification*)paramNotification
{
    //获取键盘高度
    NSValue *keyboardRectAsObject=[[paramNotification userInfo]objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect;
    [keyboardRectAsObject getValue:&keyboardRect];
    
    if (!_isTextFieldUp) {
        [UIView animateWithDuration:0.3f animations:^{
            [self.view setFrame:CGRectOffset(self.view.frame, 0, -100)];
        }];
        _isTextFieldUp = YES;
    }
}

- (void)handleKeyboardDidHidden
{
    if (_isTextFieldUp) {
        [UIView animateWithDuration:0.3f animations:^{
            [self.view setFrame:CGRectOffset(self.view.frame, 0,  100)];
        }];
        _isTextFieldUp = NO;
    }
}


- (void)handleTapGesture:(UITapGestureRecognizer*)tap{
    [self.userTextField resignFirstResponder];
    [self.passTextField resignFirstResponder];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)login{
    if ([self.userTextField.text isEqualToString:@""] || [self.passTextField.text isEqualToString:@""]) {
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"请输入用户名和密码" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alertView show];
        return;
    }
    
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:_progressHUD];
    }
    
    [_progressHUD show:YES];
    
    [ApplicationDelegate.helyEngine loginForUser:self.userTextField.text pass:self.passTextField.text completionHandler:^(BOOL isSuccess, NSArray *shops ,NSString *errMsg) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            UIStoryboard *storyboard = [UIApplication sharedApplication].keyWindow.rootViewController.storyboard;
            PhoneShopListViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhoneShopListViewController"];
            [self.navigationController pushViewController:viewController animated:YES];
        }else{
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:errMsg message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (IBAction)login:(id)sender {
    [self login];
}
@end
