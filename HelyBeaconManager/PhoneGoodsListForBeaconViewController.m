//
//  PhoneGoodsListForBeaconViewController.m
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneGoodsListForBeaconViewController.h"
#import "GoodsInfo.h"
#import "BeaconInfo.h"
#import "PhoneGoodsInfoViewController.h"
#import "GoodsCell.h"
#import "AppDelegate.h"

@interface PhoneGoodsListForBeaconViewController ()
@property (nonatomic, strong) NSArray *goods;
- (IBAction)refresh:(UIRefreshControl *)sender;

@end

@implementation PhoneGoodsListForBeaconViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.refreshControl beginRefreshing];
    [self refresh:self.refreshControl];
}

- (IBAction)refresh:(UIRefreshControl *)sender {
    
    [ApplicationDelegate.helyEngine goodsListForBeaconId:self.beaconInfo.beaconId completionHandler:^(BOOL isSuccess, NSArray *goods) {
        [self.refreshControl endRefreshing];
        if (isSuccess) {
            self.goods = [NSArray arrayWithArray:goods];
            [self.tableView reloadData];
        }
    } errorHandler:^(NSError *error) {
        [self.refreshControl endRefreshing];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.goods ? self.goods.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GoodsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsCell" forIndexPath:indexPath];
    
    GoodsInfo *goodsInfo = self.goods[indexPath.row];
    [cell setGoodsInfo:goodsInfo];
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goodsInfo"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        GoodsInfo *goodsInfo = self.goods[indexPath.row];
        [[segue destinationViewController] setGoodsInfo:goodsInfo];
    }
}

@end
