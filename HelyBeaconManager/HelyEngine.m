//
//  HelyEngine.m
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-19.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "HelyEngine.h"
#import "Utils.h"
#import "DataModel.h"

@implementation HelyEngine

-(void) loginForUser:(NSString *) user pass:(NSString *)pass completionHandler:(HelyLoginResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/manager/login.html" params:@{@"userName" : user, @"password" : [Utils md5:pass]} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            NSLog(@"shops list =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                NSMutableArray *shops = [NSMutableArray array];
                NSArray *shopDics = [jsonObject objectForKey:@"retValue"];
                for (NSDictionary *dic in shopDics) {
                    ShopInfo *shopInfo = [[ShopInfo alloc] init];
                    [shopInfo updateWithDic:dic];
                    [shops addObject:shopInfo];
                }
                [DataModel shareInstance].shops = [NSArray arrayWithArray:shops];
                [Utils saveUser:user pass:pass];
                responseBlock(YES,shops,nil);
            }else{
                responseBlock(NO,nil,[jsonObject objectForKey:@"errMsg"]);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) shopInfoForShopId:(NSString *)shopId completionHandler:(HelyShopInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/manager/interfaceForShopInfo.html" params:@{@"shopId" : shopId} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
//            NSLog(@"shops info =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                responseBlock(YES,[jsonObject objectForKey:@"retValue"]);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) updateShopInfoForShopId:(NSString*)shopId shopName:(NSString*)shopName description:(NSString*)description shopAddress:(NSString*)shopAddress tel:(NSString*)tel resData:(NSString*)resData completionHandler:(HelyUpdateShopInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (shopId) {
        [parameters setObject:shopId forKey:@"shopId"];
    }
    if (shopName) {
        [parameters setObject:shopName forKey:@"shopName"];
    }
    if (description) {
        [parameters setObject:description forKey:@"description"];
    }
    if (shopAddress) {
        [parameters setObject:shopAddress forKey:@"shopAddress"];
    }
    if (tel) {
        [parameters setObject:tel forKey:@"shopTel"];
    }
    if (resData) {
        [parameters setObject:resData forKey:@"resData"];
    }
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/manager/updateShopInfo.html" params:parameters httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
//            NSLog(@"update goods info =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                responseBlock(YES,[jsonObject objectForKey:@"retValue"]);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) goodsListForBeaconId:(NSString*)beaconId completionHandler:(HelyGoodsListForBeaconResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/manager/getGoodsListForBeacon.html" params:@{@"beaconId" : beaconId} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
//            NSLog(@"goods list =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                NSMutableArray *goods = [NSMutableArray array];
                NSArray *goodsDics = [jsonObject objectForKey:@"retValue"];
                for (NSDictionary *dic in goodsDics) {
                    GoodsInfo *goodsInfo = [[GoodsInfo alloc] init];
                    [goodsInfo updateWithDic:dic];
                    [goods addObject:goodsInfo];
                }
                responseBlock(YES,goods);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) goodsInfoForGoodsId:(NSString*)goodsId completionHandler:(HelyGoodsInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/manager/interfaceForGoodsInfo.html" params:@{@"goodsId" : goodsId} httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
//            NSLog(@"goods info =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                responseBlock(YES,[jsonObject objectForKey:@"retValue"]);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

-(void) updateGoodsInfoForGoodsId:(NSString*)goodsId goodsName:(NSString*)goodsName description:(NSString*)description price:(NSNumber*)price resData:(NSString*)resData completionHandler:(HelyUpdateGoodsInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock{
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    if (goodsId) {
        [parameters setObject:goodsId forKey:@"goodsId"];
    }
    if (goodsName) {
        [parameters setObject:goodsName forKey:@"goodsName"];
    }
    if (description) {
        [parameters setObject:description forKey:@"description"];
    }
    if (price) {
        [parameters setObject:price forKey:@"price"];
    }
    if (resData) {
        [parameters setObject:resData forKey:@"resData"];
    }
    
    MKNetworkOperation *op = [self operationWithPath:@"ibeaconServer/manager/updateGoodsInfo.html" params:parameters httpMethod:@"POST"];
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
//            NSLog(@"update goods info =%@",jsonObject);
            if ([[jsonObject objectForKey:@"errCode"] intValue] == 0) {
                responseBlock(YES,[jsonObject objectForKey:@"retValue"]);
            }else{
                responseBlock(NO,nil);
            }
        }];
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
}

@end
