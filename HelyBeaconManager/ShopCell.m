//
//  ShopCell.m
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ShopCell.h"
#import "ShopInfo.h"

@interface ShopCell(){
    ShopInfo *_shopInfo;
}

@end

@implementation ShopCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setShopInfo:(ShopInfo *)shopInfo{
    if (_shopInfo) {
        [_shopInfo removeObserver:self forKeyPath:@"shopName" context:nil];
    }
    _shopInfo = shopInfo;
    [shopInfo addObserver:self forKeyPath:@"shopName" options:NSKeyValueObservingOptionNew context:nil];
    self.textLabel.text = shopInfo.shopName;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"shopName"]) {
        self.textLabel.text = _shopInfo.shopName;
    }
}

- (void)dealloc{
    [_shopInfo removeObserver:self forKeyPath:@"shopName" context:nil];
}

@end
