//
//  PhoneUpdateViewController.m
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneUpdateViewController.h"
#import "GoodsInfo.h"
#import "ShopInfo.h"

@import QuartzCore;

@interface PhoneUpdateViewController ()
@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation PhoneUpdateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    [self setTextViewPlaceHolder];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    switch (self.updateType) {
        case UpdateTypeGoodsName:{
            self.goodsInfo.goodsName = self.textView.text;
        }
            break;
        case UpdateTypeGoodsPrice:{
            self.goodsInfo.price = [self.textView.text floatValue];
        }
            break;
        case UpdateTypeGoodsDescription:{
            self.goodsInfo.goodsDescription = self.textView.text;
        }
            break;
        case UpdateTypeShopName:{
            self.shopInfo.shopName = self.textView.text;
        }
            break;
        case UpdateTypeShopTel:{
            self.shopInfo.shopTel = self.textView.text;
        }
            break;
        case UpdateTypeShopAddress:{
            self.shopInfo.shopAddress = self.textView.text;
        }
            break;
        case UpdateTypeShopDescription:{
            self.shopInfo.shopDescription = self.textView.text;
        }
            break;
        default:
            break;
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.textView becomeFirstResponder];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.textView.layer.cornerRadius = 5;
    self.textView.layer.borderColor = [UIColor grayColor].CGColor;
    self.textView.layer.borderWidth = 1;
}

- (void)setTextViewPlaceHolder{
    switch (self.updateType) {
        case UpdateTypeGoodsName:
            self.textView.text = self.goodsInfo.goodsName;
            break;
        case UpdateTypeGoodsPrice:
            self.textView.text = [NSString stringWithFormat:@"%.2f",self.goodsInfo.price];
            break;
        case UpdateTypeGoodsDescription:
            self.textView.text = self.goodsInfo.goodsDescription;
            break;
        case UpdateTypeShopName:
            self.textView.text = self.shopInfo.shopName;
            break;
        case UpdateTypeShopTel:
            self.textView.text = self.shopInfo.shopTel;
            break;
        case UpdateTypeShopAddress:
            self.textView.text = self.shopInfo.shopAddress;
            break;
        case UpdateTypeShopDescription:
            self.textView.text = self.shopInfo.shopDescription;
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 1000) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

@end
