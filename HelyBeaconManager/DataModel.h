//
//  DataModel.h
//  HelyBeaconManager
//
//  Created by developer on 14-4-22.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataModel : NSObject
@property (nonatomic, strong) NSString *user;
@property (nonatomic, strong) NSString *pass;
@property (nonatomic, strong) NSArray *shops;

+ (instancetype)shareInstance;

@end
