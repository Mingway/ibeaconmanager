//
//  UIImage+fixOrientation.h
//  HelyBeaconManager
//
//  Created by developer on 14-5-13.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (fixOrientation)

- (UIImage *)fixOrientation;

@end
