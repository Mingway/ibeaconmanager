//
//  ShopInfo.h
//  HelyBeaconManager
//
//  Created by developer on 14-4-23.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BeaconInfo.h"

@interface ShopInfo : NSObject

@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *shopName;
@property (nonatomic, strong) NSString *shopDescription;
@property (nonatomic, assign) int favorNum;
@property (nonatomic, strong) NSURL *imageUrl;
@property (nonatomic, strong) NSString *shopAddress;
@property (nonatomic, strong) NSString *shopTel;
@property (nonatomic, strong) NSArray *beacons;
@property (nonatomic, assign) float latitude;
@property (nonatomic, assign) float longitude;

- (void)updateWithDic:(NSDictionary*)dic;

@end
