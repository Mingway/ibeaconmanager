//
//  GoodsInfo.h
//  HelyBeaconManager
//
//  Created by developer on 14-4-22.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GoodsInfo : NSObject
@property (nonatomic, strong) NSString *goodsId;
@property (nonatomic, strong) NSString *goodsName;
@property (nonatomic, strong) NSString *goodsDescription;
@property (nonatomic, assign) float price;
@property (nonatomic, assign) int favorNum;
@property (nonatomic, strong) NSURL *imageUrl;
@property (nonatomic, assign) float imageWidth;
@property (nonatomic, assign) float imageHeight;

- (void)updateWithDic:(NSDictionary*)dic;

@end
