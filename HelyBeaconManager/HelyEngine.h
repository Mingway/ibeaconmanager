//
//  HelyEngine.h
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-19.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "MKNetworkEngine.h"

#import "ShopInfo.h"
#import "GoodsInfo.h"

@interface HelyEngine : MKNetworkEngine

typedef void (^HelyLoginResponseBlock)(BOOL isSuccess ,NSArray *shops ,NSString *errMsg);
-(void) loginForUser:(NSString *) user pass:(NSString *)pass completionHandler:(HelyLoginResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyShopInfoResponseBlock)(BOOL isSuccess ,NSDictionary *shopInfoDic);
-(void) shopInfoForShopId:(NSString *)shopId completionHandler:(HelyShopInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyUpdateShopInfoResponseBlock)(BOOL isSuccess ,NSDictionary *shopInfoDic);
-(void) updateShopInfoForShopId:(NSString*)shopId shopName:(NSString*)shopName description:(NSString*)description shopAddress:(NSString*)shopAddress tel:(NSString*)tel resData:(NSString*)resData completionHandler:(HelyUpdateShopInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyGoodsListForBeaconResponseBlock)(BOOL isSuccess ,NSArray *goods);
-(void) goodsListForBeaconId:(NSString*)beaconId completionHandler:(HelyGoodsListForBeaconResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyGoodsInfoResponseBlock)(BOOL isSuccess ,NSDictionary *goodsInfoDic);
-(void) goodsInfoForGoodsId:(NSString*)goodsId completionHandler:(HelyGoodsInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;

typedef void (^HelyUpdateGoodsInfoResponseBlock)(BOOL isSuccess ,NSDictionary *goodsInfoDic);
-(void) updateGoodsInfoForGoodsId:(NSString*)goodsId goodsName:(NSString*)goodsName description:(NSString*)description price:(NSNumber*)price resData:(NSString*)resData completionHandler:(HelyUpdateGoodsInfoResponseBlock) responseBlock errorHandler:(MKNKErrorBlock) errorBlock;


@end
