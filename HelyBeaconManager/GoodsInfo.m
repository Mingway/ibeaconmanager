//
//  GoodsInfo.m
//  HelyBeaconManager
//
//  Created by developer on 14-4-22.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "GoodsInfo.h"

@implementation GoodsInfo

- (void)updateWithDic:(NSDictionary*)dic{
    if ([dic objectForKey:@"goodsId"] != [NSNull null]) {
        self.goodsId = [dic objectForKey:@"goodsId"];
    }
    if ([dic objectForKey:@"goodsName"] != [NSNull null]) {
        self.goodsName = [dic objectForKey:@"goodsName"];
    }
    if ([dic objectForKey:@"description"] != [NSNull null]) {
        self.goodsDescription = [dic objectForKey:@"description"];
    }
    if ([dic objectForKey:@"price"] != [NSNull null]) {
        self.price = [[dic objectForKey:@"price"] floatValue];
    }
    if ([dic objectForKey:@"favorNum"] != [NSNull null]) {
        self.favorNum = [[dic objectForKey:@"favorNum"]intValue];
    }
    if ([dic objectForKey:@"imageUrl"] != [NSNull null]) {
        self.imageUrl = [NSURL URLWithString:[dic objectForKey:@"imageUrl"]];
    }
    if ([dic objectForKey:@"imageWidth"] != [NSNull null]) {
        self.imageWidth = [[dic objectForKey:@"imageWidth"] floatValue];
    }
    if ([dic objectForKey:@"imageHeight"] != [NSNull null]) {
        self.imageHeight = [[dic objectForKey:@"imageHeight"] floatValue];
    }
}

@end
