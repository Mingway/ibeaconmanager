//
//  Utils.m
//  HelyBeaconManager
//
//  Created by developer on 14-4-24.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "Utils.h"
#import "keychainUtils.h"
#import "CommonCrypto/CommonDigest.h"

static NSString * const KEY_IN_KEYCHAIN = @"com.helydata.app.allinfo";
static NSString * const KEY_USER = @"com.helydata.app.user";
static NSString * const KEY_PASS = @"com.helydata.app.pass";

@implementation Utils

+ (void)saveUser:(NSString*)user pass:(NSString*)pass{
    NSMutableDictionary *KVPairs = [NSMutableDictionary dictionary];
    [KVPairs setObject:user forKey:KEY_USER];
    [KVPairs setObject:pass forKey:KEY_PASS];
    [keychainUtils save:KEY_IN_KEYCHAIN data:KVPairs];
}

+(id)read{
    NSMutableDictionary *KVPair = (NSMutableDictionary *)[keychainUtils load:KEY_IN_KEYCHAIN];
    return KVPair;
}

+ (void)delUser{
    [keychainUtils delete:KEY_IN_KEYCHAIN];
}

+(NSString *) md5: (NSString *) inPutText
{
    const char *cStr = [inPutText UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, strlen(cStr), result);
    return [[NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
             result[0], result[1], result[2], result[3],
             result[4], result[5], result[6], result[7],
             result[8], result[9], result[10], result[11],
             result[12], result[13], result[14], result[15]
             ] lowercaseString];
}

+ (BOOL)checkTel:(NSString *)str
{
    if ([str length] == 0) {
        //请输入手机号
        return NO;
    }
    //1[0-9]{10}
    //^((13[0-9])|(15[^4,\\D])|(18[0,5-9]))\\d{8}$
    //    NSString *regex = @"[0-9]{11}";
    NSString *regex = @"^((13[0-9])|(147)|(15[^4,\\D])|(18[0,5-9]))\\d{8}$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    BOOL isMatch = [pred evaluateWithObject:str];
    if (!isMatch) {
        //请输入正确的手机号
        return NO;
    }
    return YES;
}

@end
