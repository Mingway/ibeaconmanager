//
//  ShopCell.h
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopInfo;
@interface ShopCell : UITableViewCell

- (void)setShopInfo:(ShopInfo *)shopInfo;

@end
