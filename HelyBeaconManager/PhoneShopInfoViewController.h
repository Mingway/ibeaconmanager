//
//  PhoneShopInfoViewController.h
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopInfo;
@interface PhoneShopInfoViewController : UITableViewController

@property (nonatomic, strong) ShopInfo *shopInfo;

@end
