//
//  PhoneUpdateViewController.h
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, UpdateType){
    UpdateTypeGoodsName = 1,
    UpdateTypeGoodsPrice,
    UpdateTypeGoodsDescription,
    UpdateTypeShopName,
    UpdateTypeShopTel,
    UpdateTypeShopAddress,
    UpdateTypeShopDescription
};

@class GoodsInfo;
@class ShopInfo;
@interface PhoneUpdateViewController : UIViewController

@property (nonatomic, strong) GoodsInfo *goodsInfo;
@property (nonatomic, strong) ShopInfo *shopInfo;
@property (nonatomic, assign) UpdateType updateType;

@end
