//
//  DataModel.m
//  HelyBeaconManager
//
//  Created by developer on 14-4-22.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "DataModel.h"

@implementation DataModel

+ (instancetype)shareInstance{
    static DataModel *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[DataModel alloc]init];
    });
    return instance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
