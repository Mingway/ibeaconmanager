//
//  PhoneMapViewController.h
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShopInfo;
@protocol PhoneMapViewControllerDelegate <NSObject>

@required
- (void)didSelectedShop:(ShopInfo*)shopInfo;

@end

@interface PhoneMapViewController : UIViewController
@property (nonatomic, strong) NSArray *shops;
@property (nonatomic) id<PhoneMapViewControllerDelegate> delegate;
- (void)config;
@end
