//
//  PhoneShopListViewController.m
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneShopListViewController.h"
#import "DataModel.h"
#import "ShopInfo.h"
#import "PhoneBeaconListViewController.h"
#import "PhoneShopInfoViewController.h"
#import "ShopCell.h"
#import "PhoneMapViewController.h"
#import "Utils.h"

@interface PhoneShopListViewController ()<PhoneMapViewControllerDelegate ,UITableViewDataSource, UITableViewDelegate>{
    PhoneMapViewController *_mapViewController;
    BOOL _isMapViewDisplayed;
}

@property (strong, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)switchToMap:(UISegmentedControl *)sender;
- (IBAction)logoff:(UIBarButtonItem *)sender;

@end

@implementation PhoneShopListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (!_mapViewController) {
        _mapViewController = [[UIApplication sharedApplication].keyWindow.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PhoneMapViewController"];
        _mapViewController.delegate = self;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [DataModel shareInstance].shops ? [DataModel shareInstance].shops.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ShopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"shopCell" forIndexPath:indexPath];
    
    ShopInfo *shopInfo = [DataModel shareInstance].shops[indexPath.row];
    [cell setShopInfo:shopInfo];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    ShopInfo *shopInfo = [DataModel shareInstance].shops[indexPath.row];
    
    [self pushViewControllerWithShopInfo:shopInfo];
}

- (void)pushViewControllerWithShopInfo:(ShopInfo *)shopInfo{
    UIStoryboard *storyboard = [UIApplication sharedApplication].keyWindow.rootViewController.storyboard;
    PhoneShopInfoViewController *viewController = [storyboard instantiateViewControllerWithIdentifier:@"PhoneShopInfoViewController"];
    viewController.shopInfo = shopInfo;
    [self.navigationController pushViewController:viewController animated:YES];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"detail"]){
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        ShopInfo *shopInfo = [DataModel shareInstance].shops[indexPath.row];
        
        [[segue destinationViewController] setShopInfo:shopInfo];
    }
}


- (IBAction)switchToMap:(UISegmentedControl *)sender {
    if (!_isMapViewDisplayed) {
        [sender setEnabled:NO forSegmentAtIndex:0];
        [UIView transitionFromView:self.tableView toView:_mapViewController.view duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft completion:^(BOOL finished){
            _isMapViewDisplayed = YES;
            [sender setEnabled:YES forSegmentAtIndex:0];
            _mapViewController.shops = [DataModel shareInstance].shops;
            [_mapViewController config];
        }];
    }else{
        [sender setEnabled:NO forSegmentAtIndex:1];
        [UIView transitionFromView:_mapViewController.view toView:self.tableView duration:0.5 options:UIViewAnimationOptionTransitionFlipFromRight completion:^(BOOL finished){
            _isMapViewDisplayed = NO;
            [sender setEnabled:YES forSegmentAtIndex:1];
        }];
    }
}

- (IBAction)logoff:(UIBarButtonItem *)sender {
    [Utils delUser];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark MapViewControllerDelegate

- (void)didSelectedShop:(ShopInfo *)shopInfo{
    
    [self pushViewControllerWithShopInfo:shopInfo];
}

-(void)dealloc{
    _mapViewController = nil;
}

@end
