//
//  PhoneGoodsInfoViewController.m
//  HelyBeaconManager
//
//  Created by Mingway Shi on 14-5-20.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneGoodsInfoViewController.h"
#import "GoodsInfo.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "UIImage+fixOrientation.h"
#import "PhoneUpdateViewController.h"

#define KEY_NAME            @"goodsName"
#define KEY_PRICE           @"price"
#define KEY_DESCRIPTION     @"goodsDescription"

@interface PhoneGoodsInfoViewController ()<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    MBProgressHUD *_progressHUD;
    UIImagePickerController *_picker;
    GoodsInfo *_privateGoodsInfo;
    BOOL _isUpdateImage;
}

@end

@implementation PhoneGoodsInfoViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIBarButtonItem *saveBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStyleBordered target:self action:@selector(update)];
    self.navigationItem.rightBarButtonItem = saveBarButtonItem;
    
    if (!_picker) {
        _picker = [[UIImagePickerController alloc] init];
        _picker.delegate = self;
        _picker.allowsEditing = YES;
    }
    
    [self getGoodsInfo];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:KEY_NAME]) {
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([keyPath isEqualToString:KEY_PRICE]){
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }else if ([keyPath isEqualToString:KEY_DESCRIPTION]){
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void)getGoodsInfo{
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc]initWithView:self.view];
        [self.view addSubview:_progressHUD];
    }
    [_progressHUD show:YES];
    
    [ApplicationDelegate.helyEngine goodsInfoForGoodsId:self.goodsInfo.goodsId completionHandler:^(BOOL isSuccess, NSDictionary *goodsInfoDic) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            [self.goodsInfo updateWithDic:goodsInfoDic];
            
            if (!_privateGoodsInfo) {
                _privateGoodsInfo = [[GoodsInfo alloc]init];
                [_privateGoodsInfo addObserver:self forKeyPath:KEY_NAME options:NSKeyValueObservingOptionNew context:nil];
                [_privateGoodsInfo addObserver:self forKeyPath:KEY_PRICE options:NSKeyValueObservingOptionNew context:nil];
                [_privateGoodsInfo addObserver:self forKeyPath:KEY_DESCRIPTION options:NSKeyValueObservingOptionNew context:nil];
            }
            
            [_privateGoodsInfo updateWithDic:goodsInfoDic];
            
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        return 140;
    }else{
        CGFloat height = 0.0f;
        NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
        if (row == 0) {
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [_privateGoodsInfo.goodsName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }else if (row == 1){
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [[NSString stringWithFormat:@"%.2f",_privateGoodsInfo.price] boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }else if (row == 2){
            CGSize labelMaxSize = CGSizeMake(260, 1000);
            CGRect labelBounds = [_privateGoodsInfo.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            height += 30;
            height += labelBounds.size.height;
            height += 10;
        }
        
        return height;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else{
        return 3;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger section = indexPath.section;
    NSInteger row = indexPath.row;
    if (section == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCell" forIndexPath:indexPath];
        
        UIImageView *imageView = (UIImageView*)[cell viewWithTag:1];
        [imageView setImageFromURL:_privateGoodsInfo.imageUrl placeHolderImage:[UIImage imageNamed:@"placeholder.png"]];
        
        return cell;
    }else{
        if (row == 0) {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsNameCell" forIndexPath:indexPath];
            
            UILabel *nameLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [_privateGoodsInfo.goodsName boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [nameLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            nameLabel.text = _privateGoodsInfo.goodsName;
            
            return cell;
        }else if(row == 1){
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsPriceCell" forIndexPath:indexPath];
            
            NSString *price = [NSString stringWithFormat:@"%.2f",_privateGoodsInfo.price];
            UILabel *priceLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 500);
            CGRect labelBounds = [price boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [priceLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            priceLabel.text = price;
            
            return cell;
        }else{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"goodsDescriptionCell" forIndexPath:indexPath];
            
            UILabel *descriptionLabel = (UILabel*)[cell viewWithTag:1];
            NSDictionary *attribute = @{NSFontAttributeName: [UIFont systemFontOfSize:12.f]};
            CGSize labelMaxSize = CGSizeMake(260, 1000);
            CGRect labelBounds = [_privateGoodsInfo.goodsDescription boundingRectWithSize:labelMaxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attribute context:nil];
            [descriptionLabel setFrame:CGRectMake(20, 30, labelBounds.size.width, labelBounds.size.height)];
            descriptionLabel.text = _privateGoodsInfo.goodsDescription;
            
            return cell;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger section = indexPath.section;
    
    if (section == 0) {
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"选择图片来源" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"取消" otherButtonTitles:@"拍照",@"相册", nil];
        [actionSheet showInView:self.view];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -
#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:{
            
        }
            break;
        case 1:{
            //拍照
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
            if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera]) {
                sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            }
            _picker.sourceType = sourceType;
            [self presentViewController:_picker animated:YES completion:nil];
        }
            break;
        case 2:{
            //相册
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            _picker.sourceType = sourceType;
            [self presentViewController:_picker animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}

#pragma mark –
#pragma mark Camera View Delegate Methods
- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:nil];
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *newImage = [image fixOrientation];
    
    UIImageView *imageView = (UIImageView*)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:1];
    imageView.image = newImage;
    
    _isUpdateImage = YES;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark UPDATE

- (void)update{
    NSString *base64Str = nil;
    if (_isUpdateImage) {
        UIImageView *imageView = (UIImageView*)[[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]] viewWithTag:1];
        NSData *data;
        data = UIImageJPEGRepresentation(imageView.image, 0.5);
        base64Str = [data base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
    }
    
    [_progressHUD show:YES];
    
    [ApplicationDelegate.helyEngine updateGoodsInfoForGoodsId:_privateGoodsInfo.goodsId goodsName:_privateGoodsInfo.goodsName description:_privateGoodsInfo.goodsDescription price:@(_privateGoodsInfo.price) resData:base64Str completionHandler:^(BOOL isSuccess, NSDictionary *goodsInfoDic) {
        [_progressHUD hide:YES];
        if (isSuccess) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"修改成功" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
            [alertView show];
            
            [self.goodsInfo updateWithDic:goodsInfoDic];
            _isUpdateImage = NO;
        }
    } errorHandler:^(NSError *error) {
        [_progressHUD hide:YES];
    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goodsName"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeGoodsName];
        [[segue destinationViewController] setGoodsInfo:_privateGoodsInfo];
    }else if ([segue.identifier isEqualToString:@"goodsPrice"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeGoodsPrice];
        [[segue destinationViewController] setGoodsInfo:_privateGoodsInfo];
    }else if([segue.identifier isEqualToString:@"goodsDescription"]){
        [[segue destinationViewController] setUpdateType:UpdateTypeGoodsDescription];
        [[segue destinationViewController] setGoodsInfo:_privateGoodsInfo];
    }
}

-(void)dealloc{
    if (_privateGoodsInfo) {
        [_privateGoodsInfo removeObserver:self forKeyPath:KEY_NAME context:nil];
        [_privateGoodsInfo removeObserver:self forKeyPath:KEY_PRICE context:nil];
        [_privateGoodsInfo removeObserver:self forKeyPath:KEY_DESCRIPTION context:nil];
        _privateGoodsInfo = nil;
    }
}

@end
