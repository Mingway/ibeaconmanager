
//
//  ShopInfo.m
//  HelyBeaconManager
//
//  Created by developer on 14-4-23.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "ShopInfo.h"

@implementation ShopInfo

- (void)updateWithDic:(NSDictionary*)dic{
    if ([dic objectForKey:@"shopId"] != [NSNull null]) {
        self.shopId = [dic objectForKey:@"shopId"];
    }
    if ([dic objectForKey:@"shopName"] != [NSNull null]) {
        self.shopName = [dic objectForKey:@"shopName"];
    }
    if ([dic objectForKey:@"description"] != [NSNull null]) {
        self.shopDescription = [dic objectForKey:@"description"];
    }
    if ([dic objectForKey:@"favorNum"] != [NSNull null]) {
        self.favorNum = [[dic objectForKey:@"favorNum"]intValue];
    }
    if ([dic objectForKey:@"imageUrl"] != [NSNull null]) {
        self.imageUrl = [NSURL URLWithString:[dic objectForKey:@"imageUrl"]];
    }
    if ([dic objectForKey:@"shopAddress"] != [NSNull null]) {
        self.shopAddress = [dic objectForKey:@"shopAddress"];
    }
    if ([dic objectForKey:@"shopTel"] != [NSNull null]) {
        self.shopTel = [dic objectForKey:@"shopTel"];
    }
    if ([dic objectForKey:@"longitude"] != [NSNull null]) {
        self.longitude = [[dic objectForKey:@"longitude"] floatValue];
    }
    if ([dic objectForKey:@"latitude"] != [NSNull null]) {
        self.latitude = [[dic objectForKey:@"latitude"] floatValue];
    }
    if ([dic objectForKey:@"beacons"] != [NSNull null]) {
        NSMutableArray *beacons = [NSMutableArray array];
        NSArray *beaconDics = [dic objectForKey:@"beacons"];
        for (NSDictionary *dic in beaconDics) {
            BeaconInfo *beacon = [[BeaconInfo alloc]init];
            [beacon updateWithDic:dic];
            [beacons addObject:beacon];
        }
        self.beacons = beacons;
    }
}

@end
