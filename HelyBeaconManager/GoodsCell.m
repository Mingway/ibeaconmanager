//
//  GoodsCell.m
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "GoodsCell.h"
#import "GoodsInfo.h"

@interface GoodsCell(){
    GoodsInfo *_goodsInfo;
}

@end

@implementation GoodsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setGoodsInfo:(GoodsInfo *)goodsInfo{
    if (_goodsInfo) {
        [_goodsInfo removeObserver:self forKeyPath:@"goodsName" context:nil];
    }
    _goodsInfo = goodsInfo;
    [goodsInfo addObserver:self forKeyPath:@"goodsName" options:NSKeyValueObservingOptionNew context:nil];
    self.textLabel.text = goodsInfo.goodsName;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"goodsName"]) {
        self.textLabel.text = _goodsInfo.goodsName;
    }
}

- (void)dealloc{
    [_goodsInfo removeObserver:self forKeyPath:@"goodsName" context:nil];
}

@end
