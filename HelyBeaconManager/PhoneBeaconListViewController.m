//
//  PhoneBeaconListViewController.m
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "PhoneBeaconListViewController.h"
#import "ShopInfo.h"
#import "BeaconInfo.h"
#import "PhoneGoodsListForBeaconViewController.h"

@interface PhoneBeaconListViewController ()

@end

@implementation PhoneBeaconListViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shopInfo.beacons ? self.shopInfo.beacons.count : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"beaconCell" forIndexPath:indexPath];
    
    BeaconInfo *beaconInfo = self.shopInfo.beacons[indexPath.row];
    cell.textLabel.text = beaconInfo.beaconName;
    
    return cell;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"goodsListForBeacon"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        
        BeaconInfo *beaconInfo = self.shopInfo.beacons[indexPath.row];
        [[segue destinationViewController] setBeaconInfo:beaconInfo];
    }
}


@end
