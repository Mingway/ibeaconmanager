//
//  BeaconInfo.m
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import "BeaconInfo.h"

@implementation BeaconInfo

- (void)updateWithDic:(NSDictionary *)dic{
    if ([dic objectForKey:@"beaconId"] != [NSNull null]) {
        self.beaconId = [dic objectForKey:@"beaconId"];
    }
    if ([dic objectForKey:@"beaconName"]) {
        self.beaconName = [dic objectForKey:@"beaconName"];
    }
}

@end
