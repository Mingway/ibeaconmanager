//
//  BeaconInfo.h
//  HelyBeaconManager
//
//  Created by developer on 14-5-12.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BeaconInfo : NSObject
@property (nonatomic, strong) NSString *beaconId;
@property (nonatomic, strong) NSString *beaconName;

- (void)updateWithDic:(NSDictionary *)dic;

@end
