//
//  Utils.h
//  HelyBeaconManager
//
//  Created by developer on 14-4-24.
//  Copyright (c) 2014年 HelyData. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

//keychain
+ (void)saveUser:(NSString*)user pass:(NSString*)pass;
+(id)read;
+ (void)delUser;

//md5
+(NSString *) md5: (NSString *) inPutText;

//判断手机号码
+ (BOOL)checkTel:(NSString *)str;

@end
